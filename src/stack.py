class Stack(object):
    # Constructor: create an empty stack
    def __init__(self):
        self.items = list()

    # Check if the stack is empty (True) or not (False)
    def isEmpty(self):
        return not self.items

    # Add an element to the stack
    def push(self, item):
        self.items.append(item)

    # Remove last element from the stack
    def pop(self):
        if self.isEmpty():
            raise IndexError('Cannot pop from an empty stack!')
        return self.items.pop()

    # Peek into the i-th element of the stack starting from top (index=1)
    def peek(self, index=1):
        if self.isEmpty():
            raise IndexError('Cannot peek into an empty stack!')
        if self.size() == 1 and index == 2:
            raise IndexError('The stack has only one element!')
        return self.items[-index]

    # Get the size of the stack
    def size(self):
        return len(self.items)

    # Print the stack
    def print(self):
        print(self.items)
