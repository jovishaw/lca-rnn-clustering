import matplotlib.pyplot as plt
import networkx


class Node(object):
    def __init__(self, ID, parent=None, children=[]):
        self.ID = ID
        self.parent = parent
        self.children = children

    def set_parent(self, node):
        self.parent = node

    def add_child(self, node):
        self.children.append(node)

    def get_ID(self):
        return self.ID


class GeneralTree(object):
    def __init__(self, root, nodes=None, leaves=[], edges=[]):
        self.root = root
        self.nodes = [root] if not nodes else nodes
        self.leaves = leaves
        self.edges = edges

    def add_node(self, node):
        self.nodes.append(node)

    def add_leaf(self, node):
        self.leaves.append(node)

    def add_edge(self, src, dst, length):
        self.edges.append((src, dst, length))

    def get_nodes(self):
        return self.nodes

    def get_node(self, ID):
        # return node with ID or None if such a node does not exist
        return next((n for n in self.nodes if n.get_ID() == ID), None)

    def get_edges(self):
        return self.edges

    def draw(self, filename='topology.png'):
        G = networkx.Graph()
        G.add_node(self.root.ID)  # first add root
        # then add intermediate nodes
        G.add_nodes_from(n.ID for n in self.nodes[:len(self.leaves):-1])
        G.add_nodes_from(n.ID for n in self.leaves)  # finally add leaves
        G.add_edges_from((s.ID, d.ID) for (s, d, l) in self.edges)

        # Write dot file to use with graphviz
        # In bash execute "dot -T png topology.dot > alt_topology.png"
        # Requires installation of pydot
        networkx.drawing.nx_pydot.write_dot(
            G, filename.rsplit('.', 1)[0] + '.dot')

        # Same layout using matplotlib
        # Requires installation of pydot
        pos = networkx.drawing.nx_pydot.graphviz_layout(G, prog='dot')
        networkx.draw(
            G,
            pos,
            node_size=950,
            node_color='navy',
            with_labels=True,
            font_color='white',
            font_size=16)
        plt.savefig(filename)
        plt.clf()


if __name__ == '__main__':
    #################### TESTING ####################
    # Create nodes
    H0 = Node(0)
    R1 = Node(1)
    R2 = Node(2)
    H3 = Node(3)
    H4 = Node(4)
    R5 = Node(5)
    H6 = Node(6)
    H7 = Node(7)

    # Create edges between nodes
    H0.add_child(R1)
    R1.set_parent(H0)
    R1.add_child(R2)
    R1.add_child(H3)
    R2.set_parent(R1)
    R2.add_child(H4)
    R2.add_child(R5)
    H3.set_parent(R1)
    H4.set_parent(R2)
    R5.set_parent(R2)
    R5.add_child(H6)
    R5.add_child(H7)
    H6.set_parent(R5)
    H7.set_parent(R5)

    #Create binary tree
    tree = GeneralTree(H0, [H0, H3, H4, H6, H7, R5, R2, R1], [H3, H4, H6, H7], [(H0, R1, 1), (R1, R2, 2), (R1, H3, 3), (R2, H4, 4), (R2, R5, 5), (R5, H6, 6), (R5, H7, 7)])
    
    #Plot tree
    tree.draw()
