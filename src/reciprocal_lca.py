import numpy as np
import math
import stack as st
import general_tree as gt
import operator
from scipy.spatial.distance import jaccard, hamming, rogerstanimoto, kulsinski, russellrao, dice


def length_to_loss(tree):
    "Calculate link loss rate from corresponding link lenght"
    return [(s, r, 1-10**(-l)) for s, r, l in tree.get_edges()]


def length_to_jitter(tree):
    "Calculate link jitter (ms) from corresponding link lenght"
    return [(s, r, math.sqrt(l)) for s, r, l in tree.get_edges()]


def father_seq(s1, s2):
    """ Infer the outcome bit sequence of the father of two nodes.
        s1, s2: outcome bit sequences of the children """
    assert len(s1) == len(s2)
    return list(map(operator.or_, s1, s2))


def construct_lca_matrix(distances):
    dshape = distances.shape # shape of distances array
    lshape = (dshape[0]-1, dshape[1]-1) # shape of LCA array: less the source row and column
    # initialize lca matrix
    lca = np.zeros(lshape)
    # iterate over all indexes of lca matrix
    for i in range(lshape[0]):
        for j in range(i, lshape[1]):
            # source ID is assumed 0
            # i, j in lca matrix correspond to i+1, j+1 in original distances array
            lca[i,j] = max(0, (distances[0,i+1] + distances[0,j+1] - distances[i+1,j+1]) / 2)
            # LCA matrix is symmetric
            lca[j,i] = lca[i,j]
    return lca


def complete_chain(lca, mapping, nextid, chain):
    while True:
        try:
            top = chain.peek()
        except IndexError:
            # if stack is empty, start from the last created parent node
            chain.push(nextid-1)
            top = nextid-1 
        # lca index of node at the top of the stack
        i = mapping[top]
        # find lca index of maximal off-diagonal element of row i
        # maximal row element must be the diagonal element
        # assign a large value to break possible ties
        row = np.copy(lca[i,])
        row[i] = 10**6
        # find the index of the second maximal element
        j = np.argpartition(row, -2)[-2]
        # find respective node ID of maximal off-diagonal element
        nid = next(key for key, value in mapping.items() if value == j)
        # check if respective node ID is the penultimate element of the stack
        try:
            check = (nid == chain.peek(2))
        except IndexError:
            check = False
        if check: # if so terminate the chain
            break
        else: # else push the node ID in the stack
            chain.push(nid)
    #chain.print()
    r = chain.pop() # ID of one node
    l = chain.pop() # ID of other node
    return (l, r) # RNN pair: node IDs


def rec_lca(distances, threshold, formula='midpoint', verbose=False):
    # dictionary that holds different reduction formulas
    choices = { 'midpoint': lambda x, y: (x+y)/2.0,
                'maximal': lambda x, y: max(x,y) }

    #### Initialization ####
    # source ID is assumed 0 and the IDs of the destination nodes follow in ascending order
    s = gt.Node(0) # create source
    tree = gt.GeneralTree(s, nodes=None, leaves=[], edges=[]) # create tree
    for n in range(1, distances.shape[0]): # for every destination
        node = gt.Node(n) # create destination node
        tree.add_node(node) # add node to tree node set
        tree.add_leaf(node) # add node to tree leaves

    # find ID that will be used for the next created node (last used ID of the final destination node plus 1)
    nextid = tree.get_nodes()[-1].get_ID() + 1

    #### LCA depths matrix deduction ####
    lca = construct_lca_matrix(distances)
    if verbose:
        print('\nMatrix of LCA depths of destination nodes:\n')
        print(lca)

    ndest = lca.shape[0] # number of destination nodes
    # dictionary that holds the mapping of node IDs to lca matrix indexes - {nodeID: lca_index}
    mapping = {x: x-1 for x in range(1, ndest+1)}
    # initialize RNN chain: empty stack
    chain = st.Stack()
    # start the chain from node 1 (first destination node)
    chain.push(1)

    while True:
        #### Neighbor Selection ####
        lid, rid = complete_chain(lca, mapping, nextid, chain) # find reciprocal nearest neighbors
        #print(lid,rid)
        #### Reduction ####
        l = tree.get_node(lid)
        r = tree.get_node(rid)
        # create node u as the parent of the RNN pair
        u = gt.Node(ID=nextid, children=[l,r])
        rindex = mapping[rid] # lca matrix index of node r
        lindex = mapping[lid] # lca matrix index of node l
        kept = min(lindex, rindex) # index of row and column where new lca values will be stored
        removed = max(lindex, rindex) # index of row and column that will be removed from the lca matrix
        # calculate distances between parent node u and nodes l, r
        dist_ul = max(0, lca[lindex,lindex] - lca[lindex, rindex])
        dist_ur = max(0, lca[rindex,rindex] - lca[lindex, rindex])
        # calculate lca values of newly created node
        dvals = dict() # dictionary that hold new lca distances
        for k in mapping.values():
            if k != kept and k!= removed:
                if k > removed:
                    # adjust indexes that are affected by removal of rindex
                    dvals[k-1] = choices[formula](lca[kept,k], lca[removed,k])
                    #dvals[k-1] = a*lca[kept,k] + (1-a)*lca[removed,k]
                    #dvals[k-1] = max(lca[kept,k], lca[removed,k])
                else:
                    dvals[k] = choices[formula](lca[kept,k], lca[removed,k])
                    #dvals[k] = a*lca[kept,k] + (1-a)*lca[removed,k]
                    #dvals[k] = max(lca[kept,k], lca[removed,k])
            dvals[kept] =  lca[kept,removed]
        vals = [dvals[k] for k in sorted(dvals)]
        # delete column removed from lca matrix
        lca = np.delete(lca, removed, 1)
        # delete  row removed from lca matrix
        lca = np.delete(lca, removed, 0)
        # replace row kept with a new row corresponding to newly created node u
        lca[kept] = vals
        # replace column kept with a new column
        lca[:, kept] = vals
        #print(lca)
        # update dictionnary mapping
        del mapping[lid]
        del mapping[rid]
        mapping[nextid] = kept
        # update mapping of nodes to indexes that are affected by deletion of removed
        for m in range(removed+1, ndest):
            mid = next(key for key, value in mapping.items() if value == m)
            mapping[mid] = mapping[mid]-1
        # dimensions of lca matrix are reduced by 1
        ndest = ndest - 1 
        # increment for next iteration
        nextid = nextid + 1

        #### Tree reconstruction ####
        # add parent node u to the set of tree nodes
        tree.add_node(u)
        # add two new edges from u to l and r after calculating their lengths
        tree.add_edge(u, l, dist_ul)
        tree.add_edge(u, r, dist_ur)

        #### Stopping condition ####
        if lca.size == 1:
            # connect the last remaining node with source s
            tree.add_edge(s, u, 0) # zero-length edge by design
            u.parent = s
            break
        
    #### Turn binary tree into a general tree ####
    # find edges with zero length, besides the one that starts from the source
    for s, d, l in [e for e in tree.get_edges() if e[2] < threshold and e[0].get_ID() != 0]:
        # remove the terminal node of the edge from the set of tree nodes
        tree.nodes.remove(d)
        # remove the edge from the set of tree edges
        tree.edges.remove((s, d, l))
        # remove the terminal node of the edge from the list of children of the starting node of the edge
        s.children.remove(d)
        # add the children of the terminal node of the edge to the list of children of the starting node of the edge
        s.children.extend(d.children)
        # correct the edges between the ending node and each of his children by making them start from the starting node
        for c in d.children:
            i = next(index for index, item in enumerate(tree.get_edges()) if item[0] == d and item[1] == c)
            _, _, length = tree.edges.pop(i)
            tree.edges.insert(i, (s, c, length))
            c.parent = s # set parent of former d children to s
        del d

    return tree


def rec_lca_seq(distances, sequences, threshold, formula, verbose=False):
    # dictionary that holds different dissimilarity functions
    choices = { 'jaccard': jaccard,
                'hamming': hamming,
                'rogerstanimoto': rogerstanimoto, 
                'kulsinski': kulsinski,
                'russellrao': russellrao,
                'dice': dice }
    #### Initialization ####
    # source ID is assumed 0 and the IDs of the destination nodes follow in ascending order
    s = gt.Node(0) # create source
    tree = gt.GeneralTree(s, nodes=None, leaves=[], edges=[]) # create tree
    for n in range(1, distances.shape[0]): # for every destination
        node = gt.Node(n) # create destination node
        tree.add_node(node) # add node to tree node set
        tree.add_leaf(node) # add node to tree leaves

    # find ID that will be used for the next created node (last used ID of the final destination node plus 1)
    nextid = tree.get_nodes()[-1].get_ID() + 1

    #### LCA depths matrix deduction ####
    lca = construct_lca_matrix(distances)
    if verbose:
        print('\nMatrix of LCA depths of destination nodes:\n')
        print(lca)

    ndest = lca.shape[0] # number of destination nodes
    # dictionary that holds the mapping of node IDs to lca matrix indexes - {nodeID: lca_index}
    mapping = {x: x-1 for x in range(1, ndest+1)}
    # initialize RNN chain: empty stack
    chain = st.Stack()
    # start the chain from node 1 (first destination node)
    chain.push(1)

    while True:
        #### Neighbor Selection ####
        lid, rid = complete_chain(lca, mapping, nextid, chain) # find reciprocal nearest neighbors
        #print(lid,rid)
        #### Reduction ####
        l = tree.get_node(lid)
        r = tree.get_node(rid)
        # create node u as the parent of the RNN pair
        u = gt.Node(ID=nextid, children=[l,r])
        rindex = mapping[rid] # lca matrix index of node r
        lindex = mapping[lid] # lca matrix index of node l
        kept = min(lindex, rindex) # index of row and column where new lca values will be stored
        removed = max(lindex, rindex) # index of row and column that will be removed from the lca matrix
        # calculate outcome sequence of parent node u from nodes l, r
        sequences[nextid] = father_seq(sequences[lid], sequences[rid])
        # calculate distances between parent node u and nodes l, r
        dist_ul = choices[formula](sequences[nextid], sequences[lid])
        dist_ur = choices[formula](sequences[nextid], sequences[rid])
        # calculate lca values of newly created node
        dvals = dict() # dictionary that hold new lca distances
        for w, k in mapping.items():
            if k != kept and k!= removed:
                if k > removed:
                    # adjust indexes that are affected by removal of rindex
                    dvals[k-1] = (choices[formula](sequences[0], sequences[nextid]) + 
                                  choices[formula](sequences[0], sequences[w]) -
                                  choices[formula](sequences[nextid], sequences[w])) / 2.0 
                else:
                    dvals[k] = (choices[formula](sequences[0], sequences[nextid]) + 
                                choices[formula](sequences[0], sequences[w]) -
                                choices[formula](sequences[nextid], sequences[w])) / 2.0
            dvals[kept] =  lca[kept,removed]
        vals = [dvals[k] for k in sorted(dvals)]
        # delete column removed from lca matrix
        lca = np.delete(lca, removed, 1)
        # delete  row removed from lca matrix
        lca = np.delete(lca, removed, 0)
        # replace row kept with a new row corresponding to newly created node u
        lca[kept] = vals
        # replace column kept with a new column
        lca[:, kept] = vals
        #print(lca)
        # update dictionnary mapping
        del mapping[lid]
        del mapping[rid]
        mapping[nextid] = kept
        # update mapping of nodes to indexes that are affected by deletion of removed
        for m in range(removed+1, ndest):
            mid = next(key for key, value in mapping.items() if value == m)
            mapping[mid] = mapping[mid]-1
        # dimensions of lca matrix are reduced by 1
        ndest = ndest - 1 
        # increment for next iteration
        nextid = nextid + 1

        #### Tree reconstruction ####
        # add parent node u to the set of tree nodes
        tree.add_node(u)
        # add two new edges from u to l and r after calculating their lengths
        tree.add_edge(u, l, dist_ul)
        tree.add_edge(u, r, dist_ur)

        #### Stopping condition ####
        if lca.size == 1:
            # connect the last remaining node with source s
            tree.add_edge(s, u, 0) # zero-length edge by design
            print('add zero edge')
            u.parent = s
            break
        
    #### Turn binary tree into a general tree ####
    # find edges with zero length, besides the one that starts from the source
    for s, d, l in [e for e in tree.get_edges() if e[2] < threshold and e[0].get_ID() != 0]:
        # remove the terminal node of the edge from the set of tree nodes
        tree.nodes.remove(d)
        # remove the edge from the set of tree edges
        tree.edges.remove((s, d, l))
        # remove the terminal node of the edge from the list of children of the starting node of the edge
        s.children.remove(d)
        # add the children of the terminal node of the edge to the list of children of the starting node of the edge
        s.children.extend(d.children)
        # correct the edges between the ending node and each of his children by making them start from the starting node
        for c in d.children:
            i = next(index for index, item in enumerate(tree.get_edges()) if item[0] == d and item[1] == c)
            _, _, length = tree.edges.pop(i)
            tree.edges.insert(i, (s, c, length))
            c.parent = s # set parent of former d children to s
        del d

    return tree


if __name__ == '__main__':
    ########### TESTING ###########
    #distances = np.array([[0,7,9,6,7], [7,0,8,7,12], [9,8,0,9,14], [6,7,9,0,11], [7,12,14,11,0]])
    #distances = np.array([[0,8,9,8,7,7], [8,0,17,16,9,5], [9,17,0,7,16,16], [8,16,7,0,15,15], [7,9,16,15,0,8], [7,5,16,15,8,0]])
    #distances = np.array([[0, 11, 10, 9, 15], [11, 0, 3, 12, 18], [10, 3, 0, 11, 17], [9, 12, 11, 0, 8], [15, 18, 17, 8, 0]])
    #distances = np.array([[0,8,8,8,8,8], [8,0,6,10,16,16], [8,6,0,10,16,16], [8,10,10,0,16,16], [8,16,16,16,0,6], [8,16,16,16,6,0]])
    #distances = np.array([[0,6,6,6,6],[6,0,4,8,12],[6,4,0,8,12],[6,8,8,0,12],[6,12,12,12,0]])
    distances = np.array([[0,4,9,11,10,13], [4,0,11,13,12,15], [9,11,0,6,5,8], 
                          [11,13,6,0,7,10], [10,12,5,7,0,9], [13,15,8,10,9,0]])

    tree = rec_lca(distances, 0.01, verbose=True)
    tree.draw('joined.png')
    print()
    print('Nodes:', [n.get_ID() for n in tree.get_nodes()])
    for s, r, l in tree.get_edges():
        print('Edge: {} --- {} [Length: {}]'.format(s.get_ID(), r.get_ID(), l))
