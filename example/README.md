# Loss tomography example

A simple loss tomography example corresponding to the following physical routing tree:  

<img src="gen_real_topo.png" width="300"/>

Run:

`../src/tomography.py -v -t loss -sf h0_5k.txt -df h{1..6}_5k.txt -c 0.01`